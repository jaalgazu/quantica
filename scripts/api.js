// Realizar una solicitud HTTP GET para obtener el archivo JSON
fetch('https://6454ff2ea74f994b334eff5e.mockapi.io/channels')
    .then(response => response.json())//convierte en Json si es necesario
    .then(data => {
        console.log("Acá estan los datos", data) //imprimo datos de en la consola para saber la estructura y contenido
        
        const dataContainer = document.getElementById('data-container');
        dataContainer.innerHTML = ''; // Limpiar el contenido existente en el contenedor

        //Itera sobre el contendio del arreglo
        data.forEach(obj => {

            const cardElement = document.createElement('div');
            cardElement.classList.add('card'); // Agregar la clase 'card' para aplicar estilos de Bootstrap u otros frameworks

            const titleElement = document.createElement('h2');
            titleElement.textContent = obj.Title;

            const descriptionElement = document.createElement('p');
            descriptionElement.textContent = obj.Description;

            const categoryElement = document.createElement('span');
            categoryElement.textContent = obj.Category;

            const posterElement = document.createElement('img');
            posterElement.src = obj.Poster;

            const categoryText = document.createTextNode('Categoria: ');
  
            // Agregar la tarjeta al contenedor principal
            dataContainer.appendChild(cardElement);
            // Agregar elementos al contenedor
            cardElement.appendChild(titleElement);
            cardElement.appendChild(descriptionElement);
            cardElement.appendChild(categoryText);
            cardElement.appendChild(categoryElement);
            cardElement.appendChild(posterElement);                    
            
        });
    })
    .catch(error => {
        console.error('Error al obtener los datos del archivo JSON:', error); //Manejo de errores
    });
