const searchInput = document.getElementById('search-input');
const searchButton = document.getElementById('search-button');
const dataContainer = document.getElementById('data-container');
const cards = dataContainer.getElementsByClassName('card');

function filterByTitle() {
    const searchTerm = searchInput.value.toLowerCase();

    Array.from(cards).forEach(card => {
        const titleElement = card.querySelector('h2');
        const content = card.querySelector('p');
        
        const title = titleElement.textContent.toLowerCase();
        const cont = content.textContent.toLowerCase();
        
        const word = normalizeText(title, cont)

        if (word.hasOwnProperty('normalized1') && word.normalized1.includes(searchTerm) ||
            word.hasOwnProperty('normalized2') && word.normalized2.includes(searchTerm)) {
            card.style.display = 'inline-table'; // Mostrar la tarjeta si coincide con el término de búsqueda o si el término está vacío         
        } else {
            card.style.display = 'none'; // Ocultar la tarjeta si no coincide
        }
    });
    // Ocultar el campo de entrada
    searchInput.style.display = 'none';
    // Cambiar el texto del botón de búsqueda
    searchButton.textContent = 'Mostrar todos';
    // Cambiar la función de clic del botón de búsqueda
    searchButton.removeEventListener('click', filterByTitle);
    searchButton.addEventListener('click', showAll);
}

// Función que se ejecuta al hacer clic en el botón de "Mostrar todos"
function showAll() {
    Array.from(cards).forEach(card => {
        card.style.display = 'inline-table'; // Mostrar todas las tarjetas
    });
    // Restablecer el valor del campo de búsqueda
    searchInput.value = '';
    // Habilitar el campo de entrada
    searchInput.disabled = false;
    // Mostrar nuevamente el campo de entrada
    searchInput.style.display = 'block';
    // Cambiar el texto del botón de búsqueda
    searchButton.textContent = 'Buscar';
    // Cambiar la función de clic del botón de búsqueda
    searchButton.removeEventListener('click', showAll);
    searchButton.addEventListener('click', filterByTitle);
}

// Asignar la función de búsqueda al botón
searchButton.addEventListener('click', filterByTitle);

// Asignar la función de "Mostrar todos" al botón correspondiente
const showAllButton = document.getElementById('show-all-button');
if (showAllButton) { //para evitar error de codigo al no encontrarlo cuando se ejecuta la función
    showAllButton.addEventListener('click', filterByTitle);
}

function normalizeText(text1, text2) {
    // Convertir a minúsculas
    let normalized1 = text1.toLowerCase();
    let normalized2 = text2.toLowerCase();

    // Reemplazar tildes y diacríticos
    normalized1 = normalized1.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    normalized2 = normalized2.normalize("NFD").replace(/[\u0300-\u036f]/g, "");

    return { normalized1, normalized2 };
}



